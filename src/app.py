from flask import Flask,render_template,redirect,request
import random
from db import mdb

app = Flask(__name__)

@app.route('/')
def index():
    #behaneler = open('behaneler.txt')
    #behane_lines=behaneler.readlines()
    dbins = mdb()
    conn = dbins.get_con()
    cursor = conn.cursor()
    cursor.execute("SELECT behane,muellif FROM behane_generator.behaneler")
    behane_lines = cursor.fetchall()
    behane = []
    for i in range(0,len(behane_lines)):
        selected_behane=behane_lines[i]
        behane.append(selected_behane)
    result=random.choice(behane)
    #behaneler.close()
    return render_template('index.html',behane=result[0],autor=result[1])

@app.route('/behane/add/',methods=['POST'])
@app.route('/add/',methods=['POST'])
def add_behane():
    dbins = mdb()
    conn = dbins.get_con()
    cursor = conn.cursor()
    query = "INSERT INTO behane_generator.behaneler(behane,muellif) values(%s,%s)"
    if request.method == 'POST':
        print(request.form['bas'])
        print(request.form)
        if request.form['bas']=='Bas getsin':
            behane = request.form['behane']
            autor = request.form['muellif']
            if len(behane) < 2:
                return redirect("http://iskender.taryelkazimov.com/behane/")
            if len(autor) < 1:
                autor = "Anonim"
            cursor.execute(query,[behane,autor])
            conn.commit()
            cursor.close()
            conn.close()
    return redirect("http://iskender.taryelkazimov.com/behane/")

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
