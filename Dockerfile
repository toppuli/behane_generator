FROM python:alpine
LABEL maintainer="Yusif Yusifov"
RUN pip install flask
RUN apk add --no-cache mariadb-connector-c-dev ;\
    apk add --no-cache --virtual .build-deps \
        build-base \
        mariadb-dev ;\
    pip install mysqlclient;\
    apk del .build-deps 
COPY src /src/
WORKDIR /src
ENTRYPOINT ["python"]
CMD ["app.py"]

